/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by @mumei_himazin on 14/03/17.
 */
public class TwitterShare{

	public static void startActivity(Context context,Plugin plugin,TwitterIntent twitterIntent){
		context.startActivity(getIntent(plugin,twitterIntent));
	}

	public static Intent getIntent(Plugin plugin,TwitterIntent twitterIntent){
		Intent intent = plugin.intent;
		intent.putExtra(TwitterIntent.EXTRA_TWITTER_INTENT,(Parcelable)twitterIntent);
		return intent;
	}

	public static TwitterIntent getTwitterIntent(Intent intent){
		if(intent.hasExtra(TwitterIntent.EXTRA_TWITTER_INTENT)){
			Parcelable parcelable = intent.getParcelableExtra(TwitterIntent.EXTRA_TWITTER_INTENT);
			if(parcelable instanceof TwitterIntent)return (TwitterIntent)parcelable;
		}
		return null;
	}

	public static List<Plugin> findIntentSuppourtActivity(Context context,TwitterIntent twitterIntent){
		switch (twitterIntent.getType()){
			case TwitterIntent.INTENT_STATUS:
				return findStatusIntentSupportActivity(context);
			case TwitterIntent.INTENT_SEARCH:
				return findSearchIntentSupportActivity(context);
			case TwitterIntent.INTENT_USER:
				return findUserIntentSupportActivity(context);
			case TwitterIntent.INTENT_OWNER:
				return findOwnerIntentSupportActivity(context);
			default:
				return null;
		}
	}

	public static List<Plugin> findSearchIntentSupportActivity(Context context){
		return findSupportActivity(context,TwitterIntent.ACTION_SHOW_SEARCH,null);
	}
	public static List<Plugin> findUserIntentSupportActivity(Context context){
		return findSupportActivity(context,TwitterIntent.ACTION_SHOW_USER,TwitterIntent.CATEGORY_USER);
	}
	public static List<Plugin> findOwnerIntentSupportActivity(Context context){
		return findSupportActivity(context,TwitterIntent.ACTION_SHOW_USER,TwitterIntent.CATEGORY_OWNER);
	}

	public static List<Plugin> findStatusIntentSupportActivity(Context context){
		return findSupportActivity(context,TwitterIntent.ACTION_SHOW_STATUS,null);
	}

	private static List<Plugin> findSupportActivity(Context context,String action,String category){
		PackageManager pm = context.getPackageManager();
		Intent intent = new Intent(action);
		if(category!=null)intent.addCategory(category);
		List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);

		List<Plugin> plugins = new ArrayList<Plugin>();
		for(ResolveInfo resolveInfo:resolveInfos){
			Plugin plugin = new Plugin();
			plugin.label = resolveInfo.activityInfo.loadLabel(pm).toString();
			plugin.intent = new Intent(intent);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
			plugin.intent.setComponent(new ComponentName(resolveInfo.activityInfo.packageName,resolveInfo.activityInfo.name));
		}
		return plugins;
	}


}
