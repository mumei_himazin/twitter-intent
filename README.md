Twitter Intent
---------------------

 androidのTwitter関連のintentを共通のものにし  
 どのTwitterクライアントからでも利用可能なプラグインの作成を可能にする  


###導入###

build.gradleへ以下の内容を追加してください。  

plugin側  
client側  
```java
repositories {
    mavenCentral()
	maven{
		url 'http://repository.mumei-himazin.info/nexus/content/repositories/releases/'
	}
}
dependencies {
    //pluginの場合
    compile 'twitter.intent:twitter-intent-core:1.0'
}
```

client側 twitter4j(4.0.4)の場合  
```java
repositories {
    mavenCentral()
    maven{
        url 'http://repository.mumei-himazin.info/nexus/content/repositories/releases/'
    }
}
dependencies {
    //client側
    compile 'org.twitter4j:twitter4j-core:4.0.4'
    compile'twitter.intent:twitter-intent-t4j:1.0'
}
```

client側 twitter4j(4.0.4以外)の場合  
```java
repositories {
    mavenCentral()
    maven{
        url 'http://repository.mumei-himazin.info/nexus/content/repositories/releases/'
    }
}
dependencies {
    //client側
    compile 'org.twitter4j:twitter4j-core:4.0.3'
    compile('twitter.intent:twitter-intent-t4j:1.0'){
        exclude group: 'org.twitter4j', module: 'twitter4j-core'
    }
}
```

###基本###

基本となるintent  

* StatusIntent    (ツイートに関するintent)  
* OwnerIntent	(アカウントの利用者に関するintent)  
* UserIntent 	(ユーザに関するintent)  
* SearchIntent	(検索に関するintent)  


プラグインにintentを送る場合(Twitter4jなし)  
```java
StatusIntent intent = new StatusIntent.Builder()
	.setId(123123124234l)
	.setText("test")
	.setSource("<a href='http://www.aaa.com'>aaa</a>")
	.setUserId(3213123123l)
	.setUserScreenName("screen_name")
	.setUserName("??")
	.setUserDescription("??")
	.setUserLocation("???")
	.setUserUrl("url")
	.setUserProfileImageUrl(URI.create("http://pbs.twimg.com/profile_images/3343887606/675aaa9310229a2b475cbb7dd059a84f_normal.png"))
	.setCreateAt(new Date())
	.build();
TwitterShare.startActivity(getApplicationContext(),intent);
```

  
プラグインにintentを送る場合(Twitter4jあり)  
```java
Status status = ~;
StatusIntent intent = T4jIntentBuilder.buildStatusIntent(status);
TwitterShare.startActivity(getApplicationContext(),intent);
```

プラグインがintentを受ける場合  
```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="">
    <application >
		<activity name="StatusActivity">
			<intent-filter>
				<action android:name="twitterShareLibrary.lib.intent.ACTION_SHOW_STATUS" />
				<category android:name="android.intent.category.DEFAULT" />
			</intent-filter>
		</activity>
		<activity name="OwnerActivity">
			<intent-filter>
				<action android:name="twitterShareLibrary.lib.intent.ACTION_SHOW_USER" />
				<category android:name="android.intent.category.DEFAULT" />
				<category android:name="twitterShareLibrary.lib.intent.CATEGORY_OWNER" />
			</intent-filter>
		</activity>
		<activity name="UserActivity">
			<intent-filter>
				<action android:name="twitterShareLibrary.lib.intent.ACTION_SHOW_USER" />
				<category android:name="android.intent.category.DEFAULT" />
				<category android:name="twitterShareLibrary.lib.intent.CATEGORY_USER" />
			</intent-filter>
		</activity>
		<activity name="SearchActivity">
			<intent-filter>
				<action android:name="twitterShareLibrary.lib.intent.ACTION_SHOW_SEARCH" />
				<category android:name="android.intent.category.DEFAULT" />
			</intent-filter>
		</activity>
    </application>
</manifest>
```

```java
Intent intent = getIntent();
TwitterIntent twitterIntent = TwitterShare.getTwitterIntent(intent);
//instanceofの場合
if(twitterIntent instanceof StatusIntent){
	//Status
}else if(twitterIntent instanceof OwnerIntent){
	//Owner
}else if(twitterIntent instanceof UserIntent){
	//User
}else if(twitterIntent instanceof SearchIntent){
	//Search
}
//switchの場合
switch(twitterIntent.getType()){
    case TwitterIntent.INTENT_USER:
		//User
        break;
    case TwitterIntent.INTENT_OWNER:
		//Owner
        break;
    case TwitterIntent.INTENT_STATUS:
		//Status
        break;
    case TwitterIntent.INTENT_SEARCH:
		//Search
        break;
}
```


###Licence###

Copyright (C) 2014 @mumei_himazin  
     http://blog.mumei-himazin.info/  
	 
 Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);  
you may not use this file except in compliance with the License.  
You may obtain a copy of the License at  
  
     http://www.apache.org/licenses/LICENSE-2.0  
  
Unless required by applicable law or agreed to in writing, software  
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,  
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  
See the License for the specific language governing permissions and  
limitations under the License.