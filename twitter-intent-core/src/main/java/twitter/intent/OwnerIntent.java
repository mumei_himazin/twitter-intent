/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;

import android.os.Parcel;

import java.net.URI;

public class OwnerIntent implements TwitterIntent {
	private static final long serialVersionUID = -8921376916973009123L;

	String screenName;
	String name;
	long	id;
	String location;
	String url;
	String description;
	URI profileImageUrl;

	protected OwnerIntent(){
	}

	@Override
	public int getType() {
		return INTENT_OWNER;
	}

	public String getScreenName() {
		return screenName;
	}

	public String getName() {
		return name;
	}

	public long getId() {
		return id;
	}

	public String getLocation() {
		return location;
	}

	public String getUrl() {
		return url;
	}

	public String getDescription() {
		return description;
	}

	public String getProfileImageUrl() {
		return profileImageUrl.toString();
	}

	public String getProfileImageUrlMini() {
		return TwitterIntentUtils.imageUrlResize(profileImageUrl, "_mini");
	}

	public String getProfileImageUrlNormal() {
		return getProfileImageUrl();
	}

	public String getProfileImageUrlBigger() {
		return TwitterIntentUtils.imageUrlResize(profileImageUrl, "_bigger");
	}

	public String getProfileImageUrlOriginal() {
		return TwitterIntentUtils.imageUrlResize(profileImageUrl, "");
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof OwnerIntent && id==((OwnerIntent) o).id;
	}

	@Override
	public int hashCode() {
		return ((Long)id).hashCode();
	}

	@Override
	public String toString() {
		return "OwnerIntent{"+
				" id:"+id+
				" screen_name:"+screenName+
				" name:"+name+
				" url:"+url+
				" description:"+description+
				" profileImageUrl:"+ profileImageUrl +
				"}";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(screenName);
		dest.writeString(name);
		dest.writeLong(id);
		dest.writeString(location);
		dest.writeString(url);
		dest.writeString(description);
		dest.writeString(profileImageUrl.toString());
	}


	public static final Creator<OwnerIntent> CREATOR = new Creator<OwnerIntent>() {
		@Override
		public OwnerIntent createFromParcel(Parcel source) {
			return new OwnerIntent(source);
		}

		@Override
		public OwnerIntent[] newArray(int size) {
			return new OwnerIntent[size];
		}
	};

	OwnerIntent(Parcel parcel){
		screenName			= parcel.readString();
		name				= parcel.readString();
		id					= parcel.readLong();
		location			= parcel.readString();
		url					= parcel.readString();
		description			= parcel.readString();
		profileImageUrl = URI.create(parcel.readString());
	}


	public static class Builder{
		private OwnerIntent intent;

		public Builder(){
			intent = new OwnerIntent();
		}

		public Builder setScreenName(String screenName) {
			intent.screenName = screenName;
			return this;
		}

		public Builder setName(String name) {
			intent.name = name;
			return  this;
		}

		public Builder setId(long id) {
			intent.id = id;
			return this;
		}

		public Builder setLocation(String location) {
			intent.location = location;
			return this;
		}

		public Builder setUrl(String url) {
			intent.url = url;
			return this;
		}

		public Builder setDescription(String description) {
			intent.description = description;
			return this;
		}

		public Builder setProfileImageUrl(URI profileImageUrl) {
			intent.profileImageUrl = profileImageUrl;
			return this;
		}

		public OwnerIntent build(){
			return intent;
		}
	}
}
