/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;


import android.os.Parcel;

import java.net.URI;
import java.util.Date;

public class StatusIntent implements TwitterIntent {
	private static final long serialVersionUID = 812085107493483427L;

	String	text;
	long	id;
	double	latitude	= -1;
	double	longitude	= -1;
	Date	createAt;
	String	source;
	long 	inReplyToStatusId;

	String userScreenName;
	String userName;
	long userId;
	String userLocation;
	String userUrl;
	String userDescription;
	URI userProfileImageUrl;

	protected StatusIntent(){

	}

	@Override
	public int getType() {
		return INTENT_STATUS;
	}

	public String getText() {
		return text;
	}

	public long getId() {
		return id;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public String getSource() {
		return source;
	}

	public long getInReplyToStatusId() {
		return inReplyToStatusId;
	}

	public String getUserScreenName() {
		return userScreenName;
	}

	public String getUserName() {
		return userName;
	}

	public long getUserId() {
		return userId;
	}

	public String getUserLocation() {
		return userLocation;
	}

	public String getUserUrl() {
		return userUrl;
	}

	public String getUserDescription() {
		return userDescription;
	}

	public String getUserProfileImageUrl() {
		return userProfileImageUrl.toString();
	}

	public String getUserProfileImageUrlMini() {
		return TwitterIntentUtils.imageUrlResize(userProfileImageUrl, "_mini");
	}

	public String getUserProfileImageUrlNormal() {
		return getUserProfileImageUrl();
	}

	public String getUserProfileImageUrlBigger() {
		return TwitterIntentUtils.imageUrlResize(userProfileImageUrl, "_bigger");
	}

	public String getUserProfileImageUrlOriginal() {
		return TwitterIntentUtils.imageUrlResize(userProfileImageUrl, "");
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof StatusIntent && id==((StatusIntent) o).id;
	}

	@Override
	public int hashCode() {
		return ((Long)id).hashCode();
	}

	@Override
	public String toString() {
		return "StatusIntent{"+
				" id:"+id+
				" text:"+text+
				" latitude:"+ latitude+
				" longitude:"+longitude+
				" createAt:"+createAt+
				" source:"+source+
				" inReplyToStatusId:"+inReplyToStatusId+
				" userScreenName:"+userScreenName+
				" userName:"+userName+
				" userId:"+userId+
				" userLocation:"+userLocation+
				" userUrl:"+userUrl+
				" userDescription:"+userDescription+
				" userProfileImageUrl:"+userProfileImageUrl+
				"}";
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(text);
		dest.writeLong(id);
		dest.writeDouble(latitude);
		dest.writeDouble(longitude);
		dest.writeLong(createAt.getTime());
		dest.writeString(source);
		dest.writeLong(inReplyToStatusId);

		dest.writeString(userScreenName);
		dest.writeString(userName);
		dest.writeLong(userId);
		dest.writeString(userLocation);
		dest.writeString(userUrl);
		dest.writeString(userDescription);
		dest.writeString(userProfileImageUrl.toString());
	}

	public static final Creator<StatusIntent> CREATOR = new Creator<StatusIntent>() {
		@Override
		public StatusIntent createFromParcel(Parcel source) {
			return new StatusIntent(source);
		}

		@Override
		public StatusIntent[] newArray(int size) {
			return new StatusIntent[size];
		}
	};

	StatusIntent(Parcel parcel){
		text					= parcel.readString();
		id						= parcel.readLong();
		latitude				= parcel.readDouble();
		longitude				= parcel.readDouble();
		createAt = new Date(parcel.readLong());
		source					= parcel.readString();
		inReplyToStatusId = parcel.readLong();

		userScreenName = parcel.readString();
		userName = parcel.readString();
		userId = parcel.readLong();
		userLocation = parcel.readString();
		userUrl = parcel.readString();
		userDescription = parcel.readString();
		userProfileImageUrl = URI.create(parcel.readString());
	}

	public static class Builder{
		StatusIntent intent;

		public Builder(){
			intent = new StatusIntent();
		}


		public Builder setText(String text) {
			intent.text = text;
			return this;
		}

		public Builder setId(long id) {
			intent.id = id;
			return this;
		}

		public Builder setLatitude(double latitude) {
			intent.latitude = latitude;
			return this;
		}

		public Builder setLongitude(double longitude) {
			intent.longitude = longitude;
			return this;
		}

		public Builder setCreateAt(Date createAt) {
			intent.createAt = createAt;
			return this;
		}

		public Builder setSource(String source) {
			intent.source = source;
			return this;
		}

		public Builder setInReplyToStatusId(long inReplyToStatusId) {
			intent.inReplyToStatusId = inReplyToStatusId;
			return this;
		}

		public Builder setUserScreenName(String userScreenName) {
			intent.userScreenName = userScreenName;
			return this;
		}

		public Builder setUserName(String userName) {
			intent.userName = userName;
			return this;
		}

		public Builder setUserId(long userId) {
			intent.userId = userId;
			return this;
		}

		public Builder setUserLocation(String userLocation) {
			intent.userLocation = userLocation;
			return this;
		}

		public Builder setUserUrl(String userUrl) {
			intent.userUrl = userUrl;
			return this;
		}

		public Builder setUserDescription(String userDescription) {
			intent.userDescription = userDescription;
			return this;
		}

		public Builder setUserProfileImageUrl(URI userProfileImageUrl) {
			intent.userProfileImageUrl = userProfileImageUrl;
			return this;
		}

		public StatusIntent build(){
			return intent;
		}

	}


}
