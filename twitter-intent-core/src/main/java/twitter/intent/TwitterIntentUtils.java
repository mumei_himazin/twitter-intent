/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;

import java.net.URI;

public class TwitterIntentUtils {

	public static String imageUrlResize(URI profileImageUrl,String size){
		if(profileImageUrl!=null){
			String url = profileImageUrl.toString();
			int target	= url.lastIndexOf('_');
			int suffix	= url.lastIndexOf('.');
			int slash	= url.lastIndexOf('/');
			return url.substring(0,target)+size+(suffix>slash?url.substring(suffix):"");
		}
		return null;
	}
}
