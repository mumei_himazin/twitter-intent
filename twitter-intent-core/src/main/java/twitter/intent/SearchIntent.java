/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;

import android.os.Parcel;

public class SearchIntent implements TwitterIntent {
	private static final long serialVersionUID = -4746840003970272382L;

	String text;

	protected SearchIntent(){
	}

	@Override
	public int getType() {
		return INTENT_SEARCH;
	}

	private String getText(){
		return text;
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof SearchIntent && text.equals(((SearchIntent) o).text);
	}

	@Override
	public int hashCode() {
		return text.hashCode();
	}

	@Override
	public String toString() {
		return "SearchIntent{ text:"+text+"}";
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(text);
	}

	public static final Creator<SearchIntent> CREATOR = new Creator<SearchIntent>(){

		@Override
		public SearchIntent createFromParcel(Parcel source) {
			return new SearchIntent(source);
		}

		@Override
		public SearchIntent[] newArray(int size) {
			return new SearchIntent[size];
		}
	};

	SearchIntent(Parcel parcel){
		text = parcel.readString();
	}

	public class Builder{

		SearchIntent intent;

		public Builder(String text){
			intent = new SearchIntent();
			intent.text = text;
		}

		public SearchIntent build(){
			return intent;
		}
	}

}
