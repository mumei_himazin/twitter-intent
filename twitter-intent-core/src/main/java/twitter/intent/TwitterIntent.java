/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;

import android.os.Parcelable;

import java.io.Serializable;

public interface TwitterIntent extends Serializable,Parcelable {

	public static final int INTENT_USER		= 0,
							INTENT_OWNER	= 1,
							INTENT_STATUS 	= 2,
							INTENT_SEARCH	= 3;

	public static final String ACTION_SHOW_USER		= "twitter.intent.ACTION_SHOW_USER",
								ACTION_SHOW_STATUS	= "twitter.intent.ACTION_SHOW_STATUS",
								ACTION_SHOW_SEARCH	= "twitter.intent.ACTION_SHOW_SEARCH";

	public static final String CATEGORY_OWNER		= "twitter.intent.CATEGORY_OWNER",
								CATEGORY_USER		= "twitter.intent.CATEGORY_USER";

	public static final String EXTRA_TWITTER_INTENT	= "twitter.intent";

	public int getType();
}
