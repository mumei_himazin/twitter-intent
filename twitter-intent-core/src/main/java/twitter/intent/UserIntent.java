/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URI;

public class UserIntent implements TwitterIntent{
	private static final long serialVersionUID = -5984566186430022819L;

	String screenName;
	String name;
	long id;
	String location;
	String url;
	String description;
	URI profileImageUrl;

	String ownerScreenName;
	String ownerName;
	long ownerId;
	String ownerLocation;
	String ownerUrl;
	String ownerDescription;
	URI ownerProfileImageUrl;

	protected UserIntent(){
		super();
	}

	@Override
	public int getType() {
		return INTENT_USER;
	}


	public String getScreenName() {
		return screenName;
	}

	public String getName() {
		return name;
	}

	public long getId() {
		return id;
	}

	public String getLocation() {
		return location;
	}

	public String getUrl() {
		return url;
	}

	public String getDescription() {
		return description;
	}

	public String getProfileImageUrl() {
		return profileImageUrl.toString();
	}

	public String getProfileImageUrlMini() {
		return TwitterIntentUtils.imageUrlResize(profileImageUrl, "_mini");
	}

	public String getProfileImageUrlNormal() {
		return getProfileImageUrl();
	}

	public String getProfileImageUrlBigger() {
		return TwitterIntentUtils.imageUrlResize(profileImageUrl, "_bigger");
	}

	public String getProfileImageUrlOriginal() {
		return TwitterIntentUtils.imageUrlResize(profileImageUrl, "");
	}



	public String getOwnerScreenName() {
		return ownerScreenName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public long getOwnerId() {
		return ownerId;
	}

	public String getOwnerLocation() {
		return ownerLocation;
	}

	public String getOwnerUrl() {
		return ownerUrl;
	}

	public String getOwnerDescription() {
		return ownerDescription;
	}

	public String getOwnerProfileImageUrl() {
		return ownerProfileImageUrl.toString();
	}

	public String getOwnerProfileImageUrlMini() {
		return TwitterIntentUtils.imageUrlResize(ownerProfileImageUrl, "_mini");
	}

	public String getOwnerProfileImageUrlNormal() {
		return getOwnerProfileImageUrl();
	}

	public String getOwnerProfileImageUrlBigger() {
		return TwitterIntentUtils.imageUrlResize(ownerProfileImageUrl, "_bigger");
	}
	public String getOwnerProfileImageUrlOriginal() {
		return TwitterIntentUtils.imageUrlResize(ownerProfileImageUrl, "");
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(screenName);
		dest.writeString(name);
		dest.writeLong(id);
		dest.writeString(location);
		dest.writeString(url);
		dest.writeString(description);
		dest.writeString(profileImageUrl.toString());

		dest.writeString(ownerScreenName);
		dest.writeString(ownerName);
		dest.writeLong(ownerId);
		dest.writeString(ownerLocation);
		dest.writeString(ownerUrl);
		dest.writeString(ownerDescription);
		dest.writeString(ownerProfileImageUrl.toString());
	}

	public static final Parcelable.Creator<UserIntent> CREATOR = new Parcelable.Creator<UserIntent>() {
		@Override
		public UserIntent createFromParcel(Parcel source) {
			return new UserIntent(source);
		}

		@Override
		public UserIntent[] newArray(int size) {
			return new UserIntent[size];
		}
	};


	@Override
	public int describeContents() {
		return 0;
	}

	UserIntent(Parcel parcel){
		screenName			= parcel.readString();
		name				= parcel.readString();
		id					= parcel.readLong();
		location			= parcel.readString();
		url					= parcel.readString();
		description			= parcel.readString();
		profileImageUrl = URI.create(parcel.readString());

		ownerScreenName = parcel.readString();
		ownerName = parcel.readString();
		ownerId = parcel.readLong();
		ownerLocation = parcel.readString();
		ownerUrl = parcel.readString();
		ownerDescription = parcel.readString();
		ownerProfileImageUrl = URI.create(parcel.readString());
	}

	public static class Builder{
		private UserIntent intent;

		public Builder(){
			intent = new UserIntent();
		}

		public Builder setScreenName(String screenName) {
			intent.screenName = screenName;
			return this;
		}

		public Builder setName(String name) {
			intent.name = name;
			return  this;
		}

		public Builder setId(long id) {
			intent.id = id;
			return this;
		}

		public Builder setLocation(String location) {
			intent.location = location;
			return this;
		}

		public Builder setUrl(String url) {
			intent.url = url;
			return this;
		}

		public Builder setDescription(String description) {
			intent.description = description;
			return this;
		}

		public Builder setProfileImageUrl(URI profileImageUrl) {
			intent.profileImageUrl = profileImageUrl;
			return this;
		}


		public Builder setOwnerScreenName(String ownerScreenName) {
			intent.ownerScreenName = ownerScreenName;
			return this;
		}

		public Builder setOwnerName(String ownerName) {
			intent.ownerName = ownerName;
			return this;
		}

		public Builder setOwnerId(long ownerId) {
			intent.ownerId = ownerId;
			return this;
		}

		public Builder setOwnerUrl(String ownerUrl) {
			intent.ownerUrl = ownerUrl;
			return this;
		}

		public Builder setOwnerLocation(String ownerLocation) {
			intent.ownerLocation = ownerLocation;
			return this;
		}

		public Builder setOwnerDescription(String ownerDescription) {
			intent.ownerDescription = ownerDescription;
			return this;
		}

		public Builder setOwnerProfileImageUrl(URI ownerProfileImageUrl) {
			intent.ownerProfileImageUrl = ownerProfileImageUrl;
			return this;
		}

		public UserIntent build(){
			return intent;
		}
	}
}
