/*
 * Copyright (C) 2014 @mumei_himazin
 *      http://blog.mumei-himazin.info/
 *
 * Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package twitter.intent;

import twitter4j.GeoLocation;
import twitter4j.Status;
import twitter4j.User;

import java.net.URI;

/**
 * Created by @mumei_himazin on 14/03/18.
 */
public class T4jIntentBuilder {
	public static OwnerIntent buildOwnerIntent(User owner){
		OwnerIntent intent = new OwnerIntent();

		intent.screenName			= owner.getScreenName();
		intent.name					= owner.getName();
		intent.id					= owner.getId();
		intent.location				= owner.getLocation();
		intent.url					= owner.getURL();
		intent.description			= owner.getDescription();
		intent.profileImageUrl	= URI.create(owner.getProfileImageURL());

		return intent;
	}

	public static UserIntent buildUserIntent(User user, User owner){
		UserIntent intent = new UserIntent();

		intent.screenName			= user.getScreenName();
		intent.name					= user.getName();
		intent.id					= user.getId();
		intent.location				= user.getLocation();
		intent.url					= user.getURL();
		intent.description			= user.getDescription();
		intent.profileImageUrl	= URI.create(user.getProfileImageURL());

		intent.ownerScreenName			= owner.getScreenName();
		intent.ownerName					= owner.getName();
		intent.ownerId						= owner.getId();
		intent.ownerLocation				= owner.getLocation();
		intent.ownerUrl					= owner.getURL();
		intent.ownerDescription			= owner.getDescription();
		intent.ownerProfileImageUrl		= URI.create(owner.getProfileImageURL());

		return intent;
	}

	public static StatusIntent buildStatusIntent(Status status){
		StatusIntent intent = new StatusIntent();
		intent.text			= status.getText();
		intent.id			= status.getId();

		GeoLocation geoLocation = status.getGeoLocation();
		if(geoLocation!=null) {
			intent.latitude		= geoLocation.getLatitude();
			intent.longitude	= geoLocation.getLongitude();
		}

		intent.createAt				= status.getCreatedAt();
		intent.source					= status.getSource();
		intent.inReplyToStatusId	= status.getInReplyToStatusId();

		User user = status.getUser();
		intent.userScreenName			= user.getScreenName();
		intent.userName				= user.getName();
		intent.userId					= user.getId();
		intent.userLocation			= user.getLocation();
		intent.userUrl					= user.getURL();
		intent.userDescription			= user.getDescription();
		intent.userProfileImageUrl	= URI.create(user.getProfileImageURL());

		return intent;
	}
}
